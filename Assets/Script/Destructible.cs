using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructible : MonoBehaviour
{
    public GameObject destroyedVersion;
    

     /*void OnCollision()
     {
         Instantiate(destroyedVersion, transform.position, transform.rotation);
         Destroy(gameObject);
     }
     */

     void OnCollisionEnter(Collision other)
     {
         if (other.gameObject.tag=="Player")
         {
             Instantiate(destroyedVersion, transform.position, transform.rotation);
             Destroy(gameObject);
             
         }
     }

     void Update()
     {
         
     }
}
