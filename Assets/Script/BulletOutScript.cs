using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletOutScript : MonoBehaviour
{
    public GameObject Bullet;
    [SerializeField] private float TimeToFire = 0.5f;
    private float nextFire = 0f;
    void Update()
    {
        if (Time.time >= nextFire)
        {
            nextFire = Time.time + TimeToFire;
            BulletOut();
        }
    }

    void BulletOut()
    {
        GameObject bullet = Instantiate(Bullet);
        bullet.transform.position = this.transform.position;
    }
}
