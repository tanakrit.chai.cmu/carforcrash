using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreScript : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI textScore;
    void Start()
    {
    }
    
    void Update()
    {
        //ShowScore
        textScore.text = "Score: " + SingletonGameApplicationManager.Instance.scoreCollision.ToString();
    }
}
