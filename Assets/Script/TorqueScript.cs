using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorqueScript : MonoBehaviour
{
    public Vector3 torqueAxis;
    private float currentToTorque;
    public float amountToTorque = 100f;
    public float boostToTorque = 200f;
    public Vector3 force;
    public float forceMagnitude;
    
    public GameObject rearWheelLeft;
    public GameObject rearWheelRight;
    public GameObject frontWheelLeft;
    public GameObject frontWheelRight;
    void Start()
    {
        this.transform.position = SingletonGameApplicationManager.Instance.Checkpoint;
        currentToTorque = amountToTorque;
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            rearWheelLeft.GetComponent<Rigidbody>().AddRelativeTorque(torqueAxis.normalized * currentToTorque * Time.deltaTime, ForceMode.Force);
            rearWheelRight.GetComponent<Rigidbody>().AddRelativeTorque(torqueAxis.normalized * currentToTorque * Time.deltaTime, ForceMode.Force);
        }

        if (Input.GetKey(KeyCode.S))
        {

            rearWheelLeft.GetComponent<Rigidbody>().AddRelativeTorque(-torqueAxis.normalized * currentToTorque * Time.deltaTime, ForceMode.Force);
            rearWheelRight.GetComponent<Rigidbody>().AddRelativeTorque(-torqueAxis.normalized * currentToTorque * Time.deltaTime, ForceMode.Force);
        }

        if (Input.GetKey(KeyCode.A))
        {
            frontWheelLeft.GetComponent<Rigidbody>().AddRelativeForce(force * forceMagnitude * Time.deltaTime, ForceMode.Force);
            frontWheelRight.GetComponent<Rigidbody>().AddRelativeForce(force * forceMagnitude * Time.deltaTime, ForceMode.Force);
        }
        
        if (Input.GetKey(KeyCode.D))
        {
            frontWheelLeft.GetComponent<Rigidbody>().AddRelativeForce(-force * forceMagnitude * Time.deltaTime, ForceMode.Force);
            frontWheelRight.GetComponent<Rigidbody>().AddRelativeForce(-force * forceMagnitude * Time.deltaTime, ForceMode.Force);
        }

        if (Input.GetKey(KeyCode.Space))
        {
            rearWheelLeft.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            rearWheelRight.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        }

        if (Input.GetKey(KeyCode.LeftShift))
        {
            currentToTorque = boostToTorque;
        }
        else
        {
            currentToTorque = amountToTorque;
        }
        
        //PuaseKey
        if (Input.GetKeyDown(KeyCode.Escape) && !SingletonGameApplicationManager.Instance.IsKeyLockMenuActive)
        {
            SingletonGameApplicationManager.Instance.IsKeyLockMenuActive = true;
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && SingletonGameApplicationManager.Instance.IsKeyLockMenuActive)
        {
            SingletonGameApplicationManager.Instance.IsKeyLockMenuActive = false;
        }

        if (SingletonGameApplicationManager.Instance.IsKeyLockMenuActive)
        {
            Cursor.lockState = CursorLockMode.Confined;
            Time.timeScale = 0;
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
            Time.timeScale = 1;
        }
    }
}
