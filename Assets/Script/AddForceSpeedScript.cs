using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddForceSpeedScript : MonoBehaviour
{
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "SpeedZone")
        {
            Rigidbody rb = gameObject.GetComponent<Rigidbody>();
            rb.AddForce(new Vector3(0,0,250),ForceMode.Impulse);
        }
    }
}
