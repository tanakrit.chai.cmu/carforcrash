using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TimeScirpt : MonoBehaviour
{
    [SerializeField] private float TimeCount = 120;
    [SerializeField] private TextMeshProUGUI textTime;
    void Start()
    {
        
    }
    
    void Update()
    {
        TimeCount -= Time.deltaTime;
        
        //TimeShow
        textTime.text = ((int)TimeCount).ToString();

        if (TimeCount < 0)
        {
            TimeCount = 0;
            TimeIsUp();
        }
    }

    public void TimeIsUp()
    {
        SceneManager.LoadScene("SceneGameOver");
    }
}
