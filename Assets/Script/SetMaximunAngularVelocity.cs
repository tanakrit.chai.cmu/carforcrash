using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class SetMaximunAngularVelocity : MonoBehaviour
{
    Rigidbody _rigidbody;

    [SerializeField] 
    [Tooltip("Degress per second")]
    private float _maximunAngularVelocity = 720.0f;
    
    void Start()
    {
        Rigidbody rb = this.gameObject.GetComponent<Rigidbody>();
        _rigidbody = rb;

        if (rb != null)
        {
            _rigidbody.maxAngularVelocity = _maximunAngularVelocity * Mathf.Deg2Rad;
        }
    }

    public void SetMaximunAngularSpeedLimit(float val)
    {
        _maximunAngularVelocity = val * Mathf.Deg2Rad;
    }
}
