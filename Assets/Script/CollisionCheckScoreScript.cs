using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class CollisionCheckScoreScript : MonoBehaviour
{
    private int scoreUp = 100;
    
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            SingletonGameApplicationManager.Instance.scoreCollision += scoreUp;
            Destroy(GetComponent<CollisionCheckScoreScript>());
        }
    }
}
