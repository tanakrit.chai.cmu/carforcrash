using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyBulletScript : MonoBehaviour
{
    [SerializeField] private float TimeToDestroy;
    void Start()
    {
        
    }
    
    void Update()
    {
        Destroy(this.gameObject, TimeToDestroy);
    }
}
