using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuScript : MonoBehaviour
{
    [SerializeField] Button startButton;
    [SerializeField] Button optionsButton;
    [SerializeField] Button exitButton;
    
    void Start()
    {
        startButton.onClick.AddListener(delegate { StartButtonClick(startButton); });
        optionsButton.onClick.AddListener(delegate { OptionsButtonClick(optionsButton); });
        exitButton.onClick.AddListener(delegate { ExitButtonClick(exitButton); });
        Cursor.lockState = CursorLockMode.Confined;
    }

    void Update()
    {
    }

    public void StartButtonClick(Button button)
    {
        SceneManager.LoadScene("SceneSelect",LoadSceneMode.Additive);
    }

    public void OptionsButtonClick(Button button)
    {
        if (!SingletonGameApplicationManager.Instance.IsOptionsMenuActive)
        {
            SceneManager.LoadScene("SceneOptions", LoadSceneMode.Additive);
            SingletonGameApplicationManager.Instance.IsOptionsMenuActive = true;
        }
    }

    public void ExitButtonClick(Button button)
    {
        Application.Quit();
    }
}
