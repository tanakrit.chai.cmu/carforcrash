using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SelectMapScript : MonoBehaviour
{
    [SerializeField] Button mapCityButton;
    [SerializeField] Button mapAdven1Button;
    [SerializeField] Button mapAdven2Button;
    [SerializeField] Button backButton;
    
    void Start()
    {
        mapCityButton.onClick.AddListener(delegate { CityMapButtonClick(mapCityButton); });
        mapAdven1Button.onClick.AddListener(delegate { Adventure1MapButtonClick(mapAdven1Button); });
        mapAdven2Button.onClick.AddListener(delegate { Adventure2MapButtonClick(mapAdven2Button); });
        backButton.onClick.AddListener(delegate { BackButtonClick(backButton); });
    }
    
    void Update()
    {
        
    }
    
    public void CityMapButtonClick(Button button)
    {
        SceneManager.UnloadSceneAsync("SceneSelect");
        SceneManager.LoadScene("SceneGameplay");
        SingletonGameApplicationManager.Instance.Checkpoint = new Vector3(288.9f, 2.88f, 277.6f);
    }

    public void Adventure1MapButtonClick(Button button)
    {
        SceneManager.UnloadSceneAsync("SceneSelect");
        SceneManager.LoadScene("NaritRealistic");
        SingletonGameApplicationManager.Instance.Checkpoint = new Vector3(2.69f, 3.69f, -3.7f);
    }

    public void Adventure2MapButtonClick(Button button)
    {
        SceneManager.UnloadSceneAsync("SceneSelect");
        SceneManager.LoadScene("SceneGameplayRealistic");
        SingletonGameApplicationManager.Instance.Checkpoint = new Vector3(-0.1f,4.52f,-3.5f) ;
    }

    public void BackButtonClick(Button button)
    {
        SceneManager.UnloadSceneAsync("SceneSelect");
    }
}
