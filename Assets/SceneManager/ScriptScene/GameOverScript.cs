using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverScript : MonoBehaviour
{
    [SerializeField] Button restartButton;
    [SerializeField] Button backToMenuButton;
    
    void Start()
    {
        Cursor.lockState = CursorLockMode.Confined;
        restartButton.onClick.AddListener(delegate { RestartClickButton(restartButton); });
        backToMenuButton.onClick.AddListener(delegate { BackMenuClickButton(backToMenuButton); });
    }
    
    void Update()
    {
        
    }

    public void RestartClickButton(Button button)
    {
        SceneManager.UnloadSceneAsync("SceneGameOver");
        SceneManager.LoadScene("SceneGameplay");
        SingletonGameApplicationManager.Instance.scoreCollision = 0;
    }

    public void BackMenuClickButton(Button button)
    {
        SceneManager.UnloadSceneAsync("SceneGameOver");
        SceneManager.LoadScene("SceneMainMenu");
        SingletonGameApplicationManager.Instance.scoreCollision = 0;
    }
}
