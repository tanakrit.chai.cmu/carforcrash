using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseInGameScript : MonoBehaviour
{
    void Start()
    {
        
    }
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !SingletonGameApplicationManager.Instance.IsPauseMenuActive)
        {
            SceneManager.LoadScene("ScenePause",LoadSceneMode.Additive);
            SingletonGameApplicationManager.Instance.IsPauseMenuActive = true;
        }
    }
}
