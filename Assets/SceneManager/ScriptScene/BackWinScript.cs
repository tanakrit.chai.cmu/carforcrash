using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BackWinScript : MonoBehaviour
{
    [SerializeField] Button winButton;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Confined;
    }

    void Update()
    {
        winButton.onClick.AddListener(delegate { BackClickButton(winButton); });
    }

    public void BackClickButton(Button button)
    {
        SceneManager.UnloadSceneAsync("SceneWin");
        SceneManager.LoadScene("SceneMainMenu");
    }
}
